# Gift Wrap Supply Management

BoxCo is offering a twenty five percent discount on gift wrap. The staff a Holidays-R-Us want to take advantage of the offer but their budget is tight so that want to be precise. They have a list of the dimensions (length l, width w, and height h) of each present. Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating the required wrapping paper for each gift a little easier: find the surface area of the box, which is 2 * l * w + 2 * w * h + 2 * h * l. They also need a little extra paper for each present: one half the area of the smallest side.

For example:

* A present with dimensions 2x3x4 requires 2 * 6 + 2 * 12 + 2 * 8 = 52 square feet of wrapping paper plus 3 square feet of slack, for a total of 55 square feet.
* A present with dimensions 1x1x10 requires 2 * 1 + 2 * 10 + 2 * 10 = 42 square feet of wrapping paper plus 1/2 square foot of slack, for a total of 42.5 square feet.

All numbers in the list are in feet. Using the [puzzle input here](./input.txt), how many total square feet of wrapping paper should Holidays-R-Us order?

# Credits

Adapted from the [Advent of Code](https://adventofcode.com/)
